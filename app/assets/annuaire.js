
import './bootstrap';
import $ from 'jquery';
import List from 'list.js';

$( document ).ready(function() {

    var options = {
        valueNames: ['name']
    };

    var table = new List('contacts-table-container', options);

});